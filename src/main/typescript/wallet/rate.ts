
export class Rate {
    public static of(value: number): Rate {
        return new Rate(value);
    }

    private constructor(private readonly _value: number) {
    }

    public get value(): number {
        return this._value;
    }
}
