import { Currency } from './currencies';
import { Price } from './price';
import { Rate } from './rate';
import { RateProvider } from './rate_provider';
import { Stock, StockType } from './stock';

function rateFor(type: StockType, currency: Currency, provider: RateProvider): Rate {
    if (type.name === currency.iso) {
        return Rate.of(1);
    }
    return provider.rateFor(type, currency);
}

export class Wallet {
    public static empty(): Wallet {
        return new Wallet([]);
    }

    private constructor(private readonly _stocks: Stock[]) { }

    public get stocks(): Stock[] {
        return this._stocks;
    }

    public valuate(currency: Currency, provider: RateProvider): Price {
        return this.normalize().stocks
            .map((stock) => rateFor(stock.type, currency, provider).value * stock.quantity)
            .map((amount) => Price.fromNumber(currency, amount))
            .map((price) => price.round())
            .reduce(Price.add, Price.fromNumber(currency, 0));
    }

    public add(stock: Stock): Wallet {
        return new Wallet([...this._stocks, stock]);
    }

    public normalize(): Wallet {
        const content: { [key: string]: Stock } = {};
        for (const stock of this._stocks) {
            if (stock.typename in content) {
                content[stock.typename] = Stock.add(content[stock.typename], stock);
            } else {
                content[stock.typename] = stock;
            }
        }
        return new Wallet(
            [...Object.values(content)
                .sort((left, right) => StockType.compare(left.type, right.type))]);
    }
}
