import { Currency } from './currencies';
import { Rate } from './rate';

export class Price {
    public static fromNumber(currency: Currency, amount: number) {
        return new Price(currency, amount);
    }

    public static fromString(currency: Currency, amount: string) {
        return new Price(currency, parseFloat(amount));
    }

    public static add(left: Price, right: Price): Price {
        if (Currency.equals(left.currency, right.currency)) {
            return new Price(left.currency, left.amount + right.amount);
        }
        throw new Error("cannot add differents currencies");
    }

    public static equals(left: Price, right: Price): boolean {
        return left.asString() === right.asString();
    }

    private constructor(
        private readonly _currency: Currency,
        private readonly _amount: number) { }

    public get currency(): Currency {
        return this._currency;
    }

    public get amount(): number {
        return this._amount;
    }

    public round(): Price {
        return Price.fromString(this._currency, this._currency.round(this._amount));
    }

    public changeTo(currency: Currency, rate: Rate): Price {
        return new Price(currency, this._amount * rate.value);
    }

    public asString(): string {
        return `${this._currency.iso} ${this._currency.round(this._amount)}`;
    }
}
