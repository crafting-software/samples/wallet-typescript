
export class StockType {
    public static of(name: string): StockType {
        return new StockType(name);
    }

    public static equals(left: StockType, right: StockType): boolean {
        return left.name === right.name;
    }

    public static compare(left: StockType, right: StockType): number {
        if (left.name > right.name) {
            return 1;
        }
        if (left.name < right.name) {
            return -1;
        }
        return 0;
    }

    private constructor(private readonly _name: string) { }

    public stock(qty: number): Stock {
        return Stock.of(this, qty);
    }

    public get name(): string {
        return this._name;
    }
}

export class Stock {
    public static of(type: StockType, qty: number): Stock {
        return new Stock(type, qty);
    }

    public static add(left: Stock, right: Stock): Stock {
        if (StockType.equals(left.type, right.type)) {
            return new Stock(left.type, left.quantity + right.quantity);
        }
        throw new Error("cannot add differents stock type");
    }

    private constructor(private readonly _type: StockType, private readonly _quantity: number) { }

    public get type(): StockType {
        return this._type;
    }

    public get typename(): string {
        return this._type.name;
    }

    public get quantity(): number {
        return this._quantity;
    }

    public asString(): string {
        return `${this.typename}: ${this.quantity}`;
    }
}

export class StockTypes {
    public static readonly DZD = StockType.of("DZD");
    public static readonly EUR = StockType.of("EUR");
    public static readonly PETROLEUM = StockType.of("PETROLEUM");
    public static readonly USD = StockType.of("USD");
    public static readonly XBT = StockType.of("XBT");

    public static byName(name: string): StockType {
        return StockTypes[name];
    }
}
