
export class Currency {

    public static equals(left: Currency, right: Currency): boolean {
        return left.iso === right.iso;
    }

    public constructor(
        private readonly _iso: string,
        private readonly _precision: number,
        private readonly _fullname: string) { }

    public get iso(): string {
        return this._iso;
    }

    public round(value: number): string {
        return value.toFixed(this._precision);
    }
}

export class Currencies {
    public static readonly AED: Currency = new Currency("AED", 2, "United Arab Emirates dirham");
    public static readonly AFN: Currency = new Currency("AFN", 2, "Afghan afghani");
    public static readonly ALL: Currency = new Currency("ALL", 2, "Albanian lek");
    public static readonly AMD: Currency = new Currency("AMD", 2, "Armenian dram");
    public static readonly ANG: Currency = new Currency("ANG", 2, "Netherlands Antillean guilder");
    public static readonly AOA: Currency = new Currency("AOA", 2, "Angolan kwanza");
    public static readonly ARS: Currency = new Currency("ARS", 2, "Argentine peso");
    public static readonly AUD: Currency = new Currency("AUD", 2, "Australian dollar");
    public static readonly AWG: Currency = new Currency("AWG", 2, "Aruban florin");
    public static readonly AZN: Currency = new Currency("AZN", 2, "Azerbaijani manat");
    public static readonly BAM: Currency = new Currency("BAM", 2, "Bosnia and Herzegovina convertible mark");
    public static readonly BBD: Currency = new Currency("BBD", 2, "Barbados dollar");
    public static readonly BDT: Currency = new Currency("BDT", 2, "Bangladeshi taka");
    public static readonly BGN: Currency = new Currency("BGN", 2, "Bulgarian lev");
    public static readonly BHD: Currency = new Currency("BHD", 3, "Bahraini dinar");
    public static readonly BIF: Currency = new Currency("BIF", 0, "Burundian franc");
    public static readonly BMD: Currency = new Currency("BMD", 2, "Bermudian dollar");
    public static readonly BND: Currency = new Currency("BND", 2, "Brunei dollar");
    public static readonly BOB: Currency = new Currency("BOB", 2, "Boliviano");
    public static readonly BOV: Currency = new Currency("BOV", 2, "Bolivian Mvdol");
    public static readonly BRL: Currency = new Currency("BRL", 2, "Brazilian real");
    public static readonly BSD: Currency = new Currency("BSD", 2, "Bahamian dollar");
    public static readonly BTN: Currency = new Currency("BTN", 2, "Bhutanese ngultrum");
    public static readonly BWP: Currency = new Currency("BWP", 2, "Botswana pula");
    public static readonly BYN: Currency = new Currency("BYN", 2, "Belarusian ruble");
    public static readonly BZD: Currency = new Currency("BZD", 2, "Belize dollar");
    public static readonly CAD: Currency = new Currency("CAD", 2, "Canadian dollar");
    public static readonly CDF: Currency = new Currency("CDF", 2, "Congolese franc");
    public static readonly CHE: Currency = new Currency("CHE", 2, "WIR Euro (complementary currency)");
    public static readonly CHF: Currency = new Currency("CHF", 2, "Swiss franc");
    public static readonly CHW: Currency = new Currency("CHW", 2, "WIR Franc (complementary currency)");
    public static readonly CLF: Currency = new Currency("CLF", 4, "Unidad de Fomento");
    public static readonly CLP: Currency = new Currency("CLP", 0, "Chilean peso");
    public static readonly CNY: Currency = new Currency("CNY", 2, "Chinese yuan");
    public static readonly COP: Currency = new Currency("COP", 2, "Colombian peso");
    public static readonly COU: Currency = new Currency("COU", 2, "Unidad de Valor Real (UVR)");
    public static readonly CRC: Currency = new Currency("CRC", 2, "Costa Rican colon");
    public static readonly CUC: Currency = new Currency("CUC", 2, "Cuban convertible peso");
    public static readonly CUP: Currency = new Currency("CUP", 2, "Cuban peso");
    public static readonly CVE: Currency = new Currency("CVE", 0, "Cape Verde escudo");
    public static readonly CZK: Currency = new Currency("CZK", 2, "Czech koruna");
    public static readonly DASH: Currency = new Currency("DASH", 8, "Dash");
    public static readonly DJF: Currency = new Currency("DJF", 0, "Djiboutian franc");
    public static readonly DKK: Currency = new Currency("DKK", 2, "Danish krone");
    public static readonly DOP: Currency = new Currency("DOP", 2, "Dominican peso");
    public static readonly DZD: Currency = new Currency("DZD", 2, "Algerian dinar");
    public static readonly EGP: Currency = new Currency("EGP", 2, "Egyptian pound");
    public static readonly ERN: Currency = new Currency("ERN", 2, "Eritrean nakfa");
    public static readonly ETB: Currency = new Currency("ETB", 2, "Ethiopian birr");
    public static readonly ETH: Currency = new Currency("ETH", 2, "Ether");
    public static readonly EUR: Currency = new Currency("EUR", 2, "Euro");
    public static readonly FJD: Currency = new Currency("FJD", 2, "Fiji dollar");
    public static readonly FKP: Currency = new Currency("FKP", 2, "Falkland Islands pound");
    public static readonly GBP: Currency = new Currency("GBP", 2, "Pound sterling");
    public static readonly GEL: Currency = new Currency("GEL", 2, "Georgian lari");
    public static readonly GHS: Currency = new Currency("GHS", 2, "Ghanaian cedi");
    public static readonly GIP: Currency = new Currency("GIP", 2, "Gibraltar pound");
    public static readonly GMD: Currency = new Currency("GMD", 2, "Gambian dalasi");
    public static readonly GNF: Currency = new Currency("GNF", 0, "Guinean franc");
    public static readonly GTQ: Currency = new Currency("GTQ", 2, "Guatemalan quetzal");
    public static readonly GYD: Currency = new Currency("GYD", 2, "Guyanese dollar");
    public static readonly HKD: Currency = new Currency("HKD", 2, "Hong Kong dollar");
    public static readonly HNL: Currency = new Currency("HNL", 2, "Honduran lempira");
    public static readonly HRK: Currency = new Currency("HRK", 2, "Croatian kuna");
    public static readonly HTG: Currency = new Currency("HTG", 2, "Haitian gourde");
    public static readonly HUF: Currency = new Currency("HUF", 2, "Hungarian forint");
    public static readonly IDR: Currency = new Currency("IDR", 2, "Indonesian rupiah");
    public static readonly ILS: Currency = new Currency("ILS", 2, "Israeli new shekel");
    public static readonly INR: Currency = new Currency("INR", 2, "Indian rupee");
    public static readonly IOT: Currency = new Currency("IOT", 0, "IOTA");
    public static readonly IQD: Currency = new Currency("IQD", 3, "Iraqi dinar");
    public static readonly IRR: Currency = new Currency("IRR", 2, "Iranian rial");
    public static readonly ISK: Currency = new Currency("ISK", 0, "Icelandic króna");
    public static readonly JMD: Currency = new Currency("JMD", 2, "Jamaican dollar");
    public static readonly JOD: Currency = new Currency("JOD", 3, "Jordanian dinar");
    public static readonly JPY: Currency = new Currency("JPY", 0, "Japanese yen");
    public static readonly KES: Currency = new Currency("KES", 2, "Kenyan shilling");
    public static readonly KGS: Currency = new Currency("KGS", 2, "Kyrgyzstani som");
    public static readonly KHR: Currency = new Currency("KHR", 2, "Cambodian riel");
    public static readonly KMF: Currency = new Currency("KMF", 0, "Comoro franc");
    public static readonly KPW: Currency = new Currency("KPW", 2, "North Korean won");
    public static readonly KRW: Currency = new Currency("KRW", 2, "South Korean won");
    public static readonly KWD: Currency = new Currency("KWD", 3, "Kuwaiti dinar");
    public static readonly KYD: Currency = new Currency("KYD", 2, "Cayman Islands dollar");
    public static readonly KZT: Currency = new Currency("KZT", 2, "Kazakhstani tenge");
    public static readonly LAK: Currency = new Currency("LAK", 2, "Lao kip");
    public static readonly LBP: Currency = new Currency("LBP", 2, "Lebanese pound");
    public static readonly LKR: Currency = new Currency("LKR", 2, "Sri Lankan rupee");
    public static readonly LRD: Currency = new Currency("LRD", 2, "Liberian dollar");
    public static readonly LSL: Currency = new Currency("LSL", 2, "Lesotho loti");
    public static readonly LYD: Currency = new Currency("LYD", 3, "Libyan dinar");
    public static readonly MAD: Currency = new Currency("MAD", 2, "Moroccan dirham");
    public static readonly MDL: Currency = new Currency("MDL", 2, "Moldovan leu");
    public static readonly MGA: Currency = new Currency("MGA", 1, "Malagasy ariary");
    public static readonly MKD: Currency = new Currency("MKD", 2, "Macedonian denar");
    public static readonly MMK: Currency = new Currency("MMK", 2, "Myanmar kyat");
    public static readonly MNT: Currency = new Currency("MNT", 2, "Mongolian tögrög");
    public static readonly MOP: Currency = new Currency("MOP", 2, "Macanese pataca");
    public static readonly MRO: Currency = new Currency("MRO", 1, "Mauritanian ouguiya");
    public static readonly MUR: Currency = new Currency("MUR", 2, "Mauritian rupee");
    public static readonly MVR: Currency = new Currency("MVR", 2, "Maldivian rufiyaa");
    public static readonly MWK: Currency = new Currency("MWK", 2, "Malawian kwacha");
    public static readonly MXN: Currency = new Currency("MXN", 2, "Mexican peso");
    public static readonly MXV: Currency = new Currency("MXV", 2, "Mexican Unidad de Inversion (UDI)");
    public static readonly MYR: Currency = new Currency("MYR", 2, "Malaysian ringgit");
    public static readonly MZN: Currency = new Currency("MZN", 2, "Mozambican metical");
    public static readonly NAD: Currency = new Currency("NAD", 2, "Namibian dollar");
    public static readonly NGN: Currency = new Currency("NGN", 2, "Nigerian naira");
    public static readonly NIO: Currency = new Currency("NIO", 2, "Nicaraguan córdoba");
    public static readonly NOK: Currency = new Currency("NOK", 2, "Norwegian krone");
    public static readonly NPR: Currency = new Currency("NPR", 2, "Nepalese rupee");
    public static readonly NZD: Currency = new Currency("NZD", 2, "New Zealand dollar");
    public static readonly OMR: Currency = new Currency("OMR", 3, "Omani rial");
    public static readonly PAB: Currency = new Currency("PAB", 2, "Panamanian balboa");
    public static readonly PEN: Currency = new Currency("PEN", 2, "Peruvian Sol");
    public static readonly PGK: Currency = new Currency("PGK", 2, "Papua New Guinean kina");
    public static readonly PHP: Currency = new Currency("PHP", 2, "Philippine peso");
    public static readonly PKR: Currency = new Currency("PKR", 2, "Pakistani rupee");
    public static readonly PLN: Currency = new Currency("PLN", 2, "Polish złoty");
    public static readonly PYG: Currency = new Currency("PYG", 0, "Paraguayan guaraní");
    public static readonly QAR: Currency = new Currency("QAR", 2, "Qatari riyal");
    public static readonly RON: Currency = new Currency("RON", 2, "Romanian leu");
    public static readonly RSD: Currency = new Currency("RSD", 2, "Serbian dinar");
    public static readonly RUB: Currency = new Currency("RUB", 2, "Russian ruble");
    public static readonly RWF: Currency = new Currency("RWF", 0, "Rwandan franc");
    public static readonly SAR: Currency = new Currency("SAR", 2, "Saudi riyal");
    public static readonly SBD: Currency = new Currency("SBD", 2, "Solomon Islands dollar");
    public static readonly SCR: Currency = new Currency("SCR", 2, "Seychelles rupee");
    public static readonly SDG: Currency = new Currency("SDG", 2, "Sudanese pound");
    public static readonly SEK: Currency = new Currency("SEK", 2, "Swedish krona/kronor");
    public static readonly SGD: Currency = new Currency("SGD", 2, "Singapore dollar");
    public static readonly SHP: Currency = new Currency("SHP", 2, "Saint Helena pound");
    public static readonly SLL: Currency = new Currency("SLL", 2, "Sierra Leonean leone");
    public static readonly SOS: Currency = new Currency("SOS", 2, "Somali shilling");
    public static readonly SRD: Currency = new Currency("SRD", 2, "Surinamese dollar");
    public static readonly SSP: Currency = new Currency("SSP", 2, "South Sudanese pound");
    public static readonly STD: Currency = new Currency("STD", 2, "São Tomé and Príncipe dobra");
    public static readonly SVC: Currency = new Currency("SVC", 2, "Salvadoran colón");
    public static readonly SYP: Currency = new Currency("SYP", 2, "Syrian pound");
    public static readonly SZL: Currency = new Currency("SZL", 2, "Swazi lilangeni");
    public static readonly THB: Currency = new Currency("THB", 2, "Thai baht");
    public static readonly TJS: Currency = new Currency("TJS", 2, "Tajikistani somoni");
    public static readonly TMT: Currency = new Currency("TMT", 2, "Turkmenistan manat");
    public static readonly TND: Currency = new Currency("TND", 3, "Tunisian dinar");
    public static readonly TOP: Currency = new Currency("TOP", 2, "Tongan paʻanga");
    public static readonly TRY: Currency = new Currency("TRY", 2, "Turkish lira");
    public static readonly TTD: Currency = new Currency("TTD", 2, "Trinidad and Tobago dollar");
    public static readonly TWD: Currency = new Currency("TWD", 2, "New Taiwan dollar");
    public static readonly TZS: Currency = new Currency("TZS", 2, "Tanzanian shilling");
    public static readonly UAH: Currency = new Currency("UAH", 2, "Ukrainian hryvnia");
    public static readonly UGX: Currency = new Currency("UGX", 0, "Ugandan shilling");
    public static readonly USD: Currency = new Currency("USD", 3, "United States dollar");
    public static readonly USN: Currency = new Currency("USN", 2, "United States dollar (next day)");
    public static readonly UYI: Currency = new Currency("UYI", 0, "Uruguay Peso en Unidades Indexadas (URUIURUI)");
    public static readonly UYU: Currency = new Currency("UYU", 2, "Uruguayan peso");
    public static readonly UZS: Currency = new Currency("UZS", 2, "Uzbekistan som");
    public static readonly VEF: Currency = new Currency("VEF", 2, "Venezuelan bolívar");
    public static readonly VND: Currency = new Currency("VND", 0, "Vietnamese đồng");
    public static readonly VUV: Currency = new Currency("VUV", 0, "Vanuatu vatu");
    public static readonly WST: Currency = new Currency("WST", 2, "Samoan tala");
    public static readonly XAF: Currency = new Currency("XAF", 0, "CFA franc BEAC");
    public static readonly XAG: Currency = new Currency("XAG", 8, "Silver (one troy ounce)");
    public static readonly XAU: Currency = new Currency("XAU", 8, "Gold (one troy ounce)");
    public static readonly XBC: Currency = new Currency("XBC", 8, "Bitcoin Cash");
    public static readonly XBT: Currency = new Currency("XBT", 8, "Bitcoin Cash");
    public static readonly XCD: Currency = new Currency("XCD", 2, "East Caribbean dollar");
    public static readonly XLM: Currency = new Currency("XLM", 8, "Stellar Lumen");
    public static readonly XMR: Currency = new Currency("XMR", 8, "Monero");
    public static readonly XOF: Currency = new Currency("XOF", 0, "CFA franc BCEAO");
    public static readonly XPD: Currency = new Currency("XPD", 8, "Palladium (one troy ounce)");
    public static readonly XPF: Currency = new Currency("XPF", 0, "CFP franc (franc Pacifique)");
    public static readonly XPT: Currency = new Currency("XPT", 8, "Platinum (one troy ounce)");
    public static readonly XRP: Currency = new Currency("XRP", 8, "Ripple");
    public static readonly XSU: Currency = new Currency("XSU", 8, "SUCRE");
    public static readonly XZC: Currency = new Currency("XZC", 8, "Zcoin");
    public static readonly YER: Currency = new Currency("YER", 2, "Yemeni rial");
    public static readonly ZAR: Currency = new Currency("ZAR", 2, "South African rand");
    public static readonly ZEC: Currency = new Currency("ZEC", 8, "Zcash");
    public static readonly ZMW: Currency = new Currency("ZMW", 2, "Zambian kwacha");
    public static readonly ZWL: Currency = new Currency("ZWL", 2, "Zimbabwean dollar A/10");

    public static byName(name: string): Currency {
        return Currencies[name];
    }
}
