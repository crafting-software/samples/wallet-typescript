import { Currency } from './currencies';
import { Rate } from './rate';
import { StockType } from './stock';

export interface RateProvider {
    rateFor(type: StockType, currency: Currency): Rate;
}
