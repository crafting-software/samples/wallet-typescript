import * as fc from 'fast-check';

import { Currencies } from '../../../main/typescript/wallet/currencies';
import { Price } from '../../../main/typescript/wallet/price';
import { Rate } from '../../../main/typescript/wallet/rate';

describe("Price should", () => {

    function EUR(amount: number): Price {
        return Price.fromNumber(Currencies.EUR, amount);
    }

    function USD(amount: number): Price {
        return Price.fromNumber(Currencies.USD, amount);
    }

    function XBT(amount: number): Price {
        return Price.fromNumber(Currencies.XBT, amount);
    }

    it("have equality", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                (amount: number) => Price.equals(EUR(amount), EUR(amount))));
    });

    it("be addable on same currency", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                fc.integer(),
                (left: number, right: number) => Price.equals(
                    Price.add(EUR(left), EUR(right)),
                    EUR(left + right))));
    });

    it("be not addable on different currencies", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                fc.integer(),
                (left: number, right: number) =>
                    expect(() => Price.add(EUR(left), USD(right))).toThrow(Error)));
    });

    it("be renderable", () => {
        expect(EUR(2).asString()).toEqual("EUR 2.00");
        expect(EUR(2.3).asString()).toEqual("EUR 2.30");
        expect(EUR(2.431).asString()).toEqual("EUR 2.43");
        expect(EUR(2.434).asString()).toEqual("EUR 2.43");
        expect(EUR(2.435).asString()).toEqual("EUR 2.44");
        expect(EUR(2.439).asString()).toEqual("EUR 2.44");
        expect(XBT(2).asString()).toEqual("XBT 2.00000000");
        expect(XBT(2.0987654321).asString()).toEqual("XBT 2.09876543");
    });

    it("be roundable", () => {
        expect(Price.add(EUR(2.434), EUR(0.001)).asString()).toEqual("EUR 2.44");
        expect(Price.add(EUR(2.434).round(), EUR(0.001)).asString()).toEqual("EUR 2.43");
    });

    it("be changable", () => {
        expect(EUR(100).changeTo(Currencies.USD, Rate.of(0.5))).toEqual(USD(50));
    });

});
