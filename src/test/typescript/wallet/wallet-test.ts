import { Currencies } from '../../../main/typescript/wallet/currencies';
import { Price } from '../../../main/typescript/wallet/price';
import { RateProvider } from '../../../main/typescript/wallet/rate_provider';
import { StockTypes } from '../../../main/typescript/wallet/stock';
import { Wallet } from '../../../main/typescript/wallet/wallet';
import { FixedRateProvider } from './fixed_rate_provider';

describe("Wallet", () => {
    it("empty wallet should be normalized to empty", () => {
        expect(render(wallet().normalize())).toEqual("");
    });

    it("wallet should be normalized without loosing content", () => {
        expect(render(wallet("EUR: 10").normalize())).toEqual("EUR: 10");
    });

    it("wallet should be normalized ordered by stock type", () => {
        expect(render(wallet("USD: 20", "EUR: 10").normalize())).toEqual("EUR: 10; USD: 20");
    });

    it("wallet should be normalized concataining same stock type", () => {
        expect(render(wallet("EUR: 20", "EUR: 10").normalize())).toEqual("EUR: 30");
    });
});

describe("Wallet functional", () => {

    it("empty wallet should value zero in euro", () => {
        expect(amountInEuro(wallet())).toEqual("EUR 0.00");
    });

    it("wallet with stock in euros should sum of values in euro", () => {
        expect(amountInEuro(wallet("EUR: 100"))).toEqual("EUR 100.00");
    });

    it("wallet with stock in different currency should use change", () => {
        expect(amountInEuro(wallet("USD: 100"), rates("USD/EUR 1.5"))).toEqual("EUR 150.00");
    });

    it("wallet with stock in different currencies should use change", () => {
        expect(amountInEuro(
            wallet("USD: 100", "XBT: 0.003", "DZD: 1000"),
            rates("USD/EUR 1.5", "DZD/EUR 0.01", "XBT/EUR 4000.00")))
            .toEqual("EUR 172.00");
    });

    it("wallet each stock should be rounded independantly", () => {
        expect(amountInEuro(
            wallet("USD: 10.44", "DZD: 10.44"),
            rates("USD/EUR 0.1", "DZD/EUR 0.1")))
            .toEqual("EUR 2.08");
    });

    it("wallet should be normalized before valuating", () => {
        expect(amountInEuro(
            wallet("USD: 10.44", "USD: 10.44"),
            rates("USD/EUR 0.1")))
            .toEqual("EUR 2.09");
    });

    function rates(...definitions: string[]): RateProvider {
        return FixedRateProvider.rates(...definitions);
    }

    function amountInEuro(subject: Wallet, provider?: RateProvider): string {
        return subject
            .valuate(
                Currencies.EUR,
                provider !== undefined ? provider : FixedRateProvider.empty())
            .asString();
    }
});

describe("Wallet gherkin", () => {

    it("empty wallet should value zero in euro", () => {
        given_an_empty_wallet()
            .when_I_evaluate_in_EUR()
            .then_the_price_should_be("EUR 0.00");
    });

    it("wallet with stock in euros should sum of values in euro", () => {
        given_a_wallet_with("EUR: 100")
            .when_I_evaluate_in_EUR()
            .then_the_price_should_be("EUR 100.00");
    });

    it("wallet with stock in different currency should use change", () => {
        given_a_wallet_with("USD: 100")
            .and_exchange_rates("USD/EUR 1.5")
            .when_I_evaluate_in_EUR()
            .then_the_price_should_be("EUR 150.00");
    });

    it("wallet with stock in different currencies should use change", () => {
        given_a_wallet_with("USD: 100", "XBT: 0.003", "DZD: 1000")
            .and_exchange_rates("USD/EUR 1.5", "DZD/EUR 0.01", "XBT/EUR 4000.00")
            .when_I_evaluate_in_EUR()
            .then_the_price_should_be("EUR 172.00");
    });

    class Context {
        private price: Price = Price.fromNumber(Currencies.EUR, 0);
        private provider: RateProvider = FixedRateProvider.empty();

        public constructor(private subject: Wallet) { }

        public and_exchange_rates(...rates: string[]): Context {
            this.provider = FixedRateProvider.rates(...rates);
            return this;
        }

        public when_I_evaluate_in_EUR(): Context {
            this.price = this.subject.valuate(Currencies.EUR, this.provider);
            return this;
        }

        public then_the_price_should_be(price: string) {
            expect(this.price.asString()).toEqual(price);
        }
    }

    function given_an_empty_wallet() {
        return new Context(Wallet.empty());
    }

    function given_a_wallet_with(...definitions: string[]) {
        return new Context(wallet(...definitions));
    }
});

function wallet(...definitions: string[]): Wallet {
    const pattern = new RegExp("([A-Z]+): (.+)");
    let result = Wallet.empty();
    for (const def of definitions) {
        const matcher = pattern.exec(def);
        if (matcher !== null) {
            result = result.add(StockTypes.byName(matcher[1]).stock(parseFloat(matcher[2])));
        }
    }
    return result;
}

function render(subject: Wallet): string {
    return subject.stocks
        .map((stock) => stock.asString())
        .join("; ");
}
