import * as fc from 'fast-check';

import { Stock, StockType, StockTypes } from '../../../main/typescript/wallet/stock';

describe("StockType should", () => {

    it("have equality", () => {
        fc.assert(
            fc.property(
                fc.string(),
                (name: string) => StockType.equals(StockType.of(name), StockType.of(name))));
    });

    it("be comparable, same values", () => {
        fc.assert(
            fc.property(
                fc.string(),
                (name: string) => StockType.compare(StockType.of(name), StockType.of(name)) === 0));
    });

    it("be comparable, different values", () => {
        expect(StockType.compare(StockTypes.EUR, StockTypes.USD)).toEqual(-1);
        expect(StockType.compare(StockTypes.USD, StockTypes.EUR)).toEqual(1);
    });

});

describe("StockType should", () => {

    function EUR(amount: number): Stock {
        return StockTypes.EUR.stock(amount);
    }

    function PETROLEUM(amount: number): Stock {
        return StockTypes.PETROLEUM.stock(amount);
    }

    it("be addable on same stock type", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                fc.integer(),
                (left: number, right: number) =>
                    Stock.add(EUR(left), EUR(right)).asString()
                    ===
                    EUR(left + right).asString()));
    });

    it("be not addable on different currencies", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                fc.integer(),
                (left: number, right: number) =>
                    expect(() => Stock.add(EUR(left), PETROLEUM(right))).toThrow(Error)));
    });

});
