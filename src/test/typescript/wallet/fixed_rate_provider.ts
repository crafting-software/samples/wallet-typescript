import { Currencies, Currency } from '../../../main/typescript/wallet/currencies';
import { Rate } from '../../../main/typescript/wallet/rate';
import { RateProvider } from '../../../main/typescript/wallet/rate_provider';
import { StockType } from '../../../main/typescript/wallet/stock';

function key(type: StockType, currency: Currency): string {
    return `${type.name}/${currency.iso}`;
}

const RATE_DEFINITION: RegExp = new RegExp("([A-Z]+)/([A-Z]+)\\s+(.+)");

export class FixedRateProvider implements RateProvider {
    public static rates(...rates: string[]): FixedRateProvider {
        let provider = FixedRateProvider.empty();
        for (const definition of rates) {
            const matcher = RATE_DEFINITION.exec(definition);
            provider = provider.update(
                StockType.of(matcher[1]),
                Currencies.byName(matcher[2]),
                Rate.of(parseFloat(matcher[3])));
        }
        return provider;
    }

    public static empty(): FixedRateProvider {
        return new FixedRateProvider({});
    }

    private constructor(private readonly _rates: { [key: string]: Rate }) { }

    public rateFor(type: StockType, currency: Currency): Rate {
        return this._rates[key(type, currency)];

    }

    public update(type: StockType, currency: Currency, rate: Rate): FixedRateProvider {
        const copy = { ...this._rates };
        copy[key(type, currency)] = rate;
        return new FixedRateProvider(copy);
    }
}
